﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bossTimerr : MonoBehaviour {

    public GameObject dialogueBox;
    public float count = 0;
    // Use this for initialization
    void Start()
    {
        dialogueBox.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (count <= 3)
        {
            count = count + Time.deltaTime;
        }

        if (count >= 3)
        {
            dialogueBox.SetActive(true);
        }
    }
}
