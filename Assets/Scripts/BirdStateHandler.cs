﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdStateHandler : MonoBehaviour
{
    public Animator bird;
    public GameObject camera;
   


    public void playGameplayMusic()
    {
        camera.GetComponent<TransitionHandler>().playGameplaySound();
    }

    public void playChirp()
    {
        camera.GetComponent<TransitionHandler>().playChirpSound();
    }

    public void returnToDefault()
    {
        bird.SetBool("fly",false);
    }

    public void OnMouseDown()
    {
        if (QuestManager.instance.hasClearedQuestShootBirds == false)
        {
            bird.gameObject.GetComponent<Animator>().SetBool("fly", true);
        }
        else
        {
            StartCoroutine("playMusic");
        }
    }

    public IEnumerator playMusic()
    {
        camera.GetComponent<TransitionHandler>().playBirdSing();
        this.transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        this.transform.GetChild(0).gameObject.SetActive(false);
    }

}
