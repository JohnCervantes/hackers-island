﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bossDialogue : MonoBehaviour {


    public Text text;
    private string[] dialogue = new string[4];
    int count;
   

    private void Start()
    {
        count = 0;
        set();
        InvokeRepeating("changeDialogue", 0.0f, 5.0f);
    }

    void set()
    {
      
        dialogue[0] = "I'm the best hacker!";
        dialogue[1] = "heh";
        dialogue[2] = "ha ha ha! ha ha ha! ha ha ha! ha ha ha! ha ha ha ha! ha ha ha!";
        dialogue[3] = " ";

    }

    void changeDialogue()
    {
        if (count == 2)
        {
            StartCoroutine(textScroll2(dialogue[count]));
        }
        else
        {
            StartCoroutine(textScroll(dialogue[count]));
        }
        

        count++;


        if (count == 4)
        {
            count = 0;
        }
    }


    public IEnumerator textScroll(string lineOfText)
    {
        int letter = 0;
        text.text = "";

        while (letter <= lineOfText.Length - 1)
        {
            text.text = text.text + lineOfText[letter];
            letter++;
            yield return new WaitForSeconds(0.10f);
        }

    }

    public IEnumerator textScroll2(string lineOfText)
    {
        int letter = lineOfText.Length-1;
        text.text = "";

        while (letter >= 0)
        {
            text.text =  lineOfText[letter] + text.text;
            letter--;
            yield return new WaitForSeconds(0.05f);
        }

    }
}
