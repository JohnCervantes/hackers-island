﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class fiveHours : MonoBehaviour
{

    public Text texts;
    public GameObject image;
    public string textsFiveHours;
    public GameObject rain;
    public AudioSource source;
    public AudioSource keyboardSource;
    public AudioClip keyboardSound;
    public Image black;

    // Use this for initialization
    void Start()
    {
        image.GetComponentInChildren<Text>().enabled = false;
        textsFiveHours = "Five hours earlier . . .";
        keyboardSource.clip = keyboardSound;
        image.gameObject.SetActive(false);

    }

    public void loadFiveHours()
    {
        image.gameObject.SetActive(true);
        StartCoroutine("fadeOut");
    }



    public IEnumerator textScroll(string lineOfText)
    {
        int letter = 0;
        texts.text = "";
        keyboardSource.Play();

        while (letter <= lineOfText.Length - 1)
        {
            texts.text = texts.text + lineOfText[letter];
            letter++;
            yield return new WaitForSeconds(0.10f);

        }
        keyboardSource.Stop();
    }

    public IEnumerator timer()
    {
        yield return new WaitForSeconds(6);
        SceneManager.LoadScene("islandDay");
    }

    public IEnumerator fadeOut()
    {
        yield return new WaitUntil(() => black.color.a == 1);
        image.GetComponentInChildren<Text>().enabled = true;
        rain.gameObject.SetActive(false);
        source.GetComponent<AudioSource>().enabled = false;
        StartCoroutine(textScroll(textsFiveHours));
        StartCoroutine(timer());
    }
}