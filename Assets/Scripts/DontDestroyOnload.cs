﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnload : MonoBehaviour {

    private DontDestroyOnload instance;
   
    void Awake()
    {
        instance = this;
            DontDestroyOnLoad(instance.gameObject);  
    }
}

