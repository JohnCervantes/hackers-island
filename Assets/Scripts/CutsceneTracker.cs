﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTracker : MonoBehaviour
{

    public static CutsceneTracker instance;
    public bool hasPlayedCutscene;
    public bool canEnterCastle;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void disableCutscene()
    {
        hasPlayedCutscene = true;
    }

    public void enterTheCastle()
    {
        canEnterCastle = true;
    }


}
