﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class slotHandler : MonoBehaviour
{
    private Vector3 origPos;
    public Sprite blank;

	// Use this for initialization
	void Start () {
		origPos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
	}

    public void reset()
    {
        this.transform.position = origPos;
        this.GetComponent<Image>().sprite = blank;
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
    }
}
