﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EldritchQuest : MonoBehaviour {

    public GameObject eldritchQuest;

    public void OnMouseDown()
    {
        eldritchQuest.SetActive(true);

    }
    
    void enableForteQuest()
    {
        eldritchQuest.SetActive(true);
    }

    void disableForteQuest()
    {
        eldritchQuest.SetActive(false);
    }
}
