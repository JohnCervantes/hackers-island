﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public bool timesUp;
    public GameObject wifiNetwork;
    public GameObject cleared;
    public float length;
    public Text text;
    public Text addOrSubtract;
    public Text connections;


    void Start()
    {
        StartCoroutine("startTimer");
    }

    public IEnumerator startTimer()
    {
        resetTimer();     
        yield return new WaitForSeconds(1);     
        while (length >= 0)
        {
            text.text = length.ToString("F").Replace(".", ":");
            length = length - 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        timesUp = true;
        text.text = "0:00";
        wifiNetwork.GetComponent<WIFINetworkManager>().endTheGame();
        this.GetComponent<Animator>().Play("TimerTimesUp");
        string k = " Total connections Secured: <size=70><b>" + wifiNetwork.GetComponent<WIFINetworkManager>().secured.ToString() + "</b></size>";
        connections.text = k;
    }

    public void resetTimer()
    {
        timesUp = false;
        text.text = length.ToString("F").Replace(".", ":");
        wifiNetwork.GetComponent<WIFINetworkManager>().secured = 0;
    }

    public void addSecond()
    {
        if (timesUp == false)
        {
            length = length + 1;
            addOrSubtract.GetComponent<Text>().color = Color.green;
            addOrSubtract.text = "+1";
            this.GetComponent<Animator>().SetTrigger("change");
        }
    }

    public void Cleared()
    {
        cleared.GetComponent<AddScore>().score = wifiNetwork.GetComponent<WIFINetworkManager>().secured * 20;
        cleared.GetComponent<AddScore>().playCleared();
    }

    public void minusSecond()
    {
        if (timesUp == false)
        {
            length = length - 1;
            addOrSubtract.GetComponent<Text>().color = Color.white;
            addOrSubtract.text = "-1";
            this.GetComponent<Animator>().SetTrigger("change");
        }
    }
}